/*
L1=>Encender Led
L0=>Apagar Led
MD=>Gira Motor XXX pasos a la derecha
MI=>Gira Motor XXX pasos a la izquierda
MS=>Detener motor
*/

#include <BluetoothSerial.h>

int pinLed = 18;
int pinA = 23;
int pinB = 22;
int pinC = 21;
int pinD = 19;
BluetoothSerial SerialBT;
String comando;
String secuenciaMotor = "";
byte pos = 0;

bool secuencia[][4] = { 
	{true,false,false,false},
	{false,true,false,false},
	{false,false,true,false},
	{false,false,false,true}
};

// the setup function runs once when you press reset or power the board
void setup() {
	SerialBT.begin("ESP32");
	pinMode(pinLed, OUTPUT);
	pinMode(pinA, OUTPUT);
	pinMode(pinB, OUTPUT);
	pinMode(pinC, OUTPUT);
	pinMode(pinD, OUTPUT);
	
}

// the loop function runs over and over again until power down or reset
void loop() {
	if (SerialBT.available()) {
		comando = SerialBT.readStringUntil('\n');
		if (comando == "L1") {
			Led(true);
		}
		if (comando == "L0") {
			Led(false);
		}
		if (comando == "MI") {
			secuenciaMotor = "Izq";
		}
		if (comando[0] == 'M') {
			if (comando[1] == 'I') {
				secuenciaMotor = "Izq";
			}
			if (comando[1] == 'D') {
				secuenciaMotor = "Der";
			}
			if (comando[1] == 'S') {
				secuenciaMotor = "Alto";
			}
		}
	}
	if (secuenciaMotor != "") {
		if (secuenciaMotor == "Izq") {
			pos--;
			if (pos == -1) {
				pos = 3;
			}
		}
		if (secuenciaMotor == "Der") {
			pos++;
			if (pos == 4) {
				pos = 0;
			}
		}
	}
	Paso(secuencia[pos][0], secuencia[pos][1], secuencia[pos][2], secuencia[pos][3]);
	delay(50);
}

void Led(bool encendido) {
	if (encendido) {
		digitalWrite(pinLed, HIGH);
		SerialBT.println("Led Encendido");
	}
	else {
		digitalWrite(pinLed, LOW);
		SerialBT.println("Led Apagado");
	}
}

void Paso(bool a, bool b, bool c, bool d) {
	if (a) {
		digitalWrite(pinA, HIGH);
	}
	else {
		digitalWrite(pinA, LOW);
	}
	if (b) {
		digitalWrite(pinB, HIGH);
	}
	else {
		digitalWrite(pinB, LOW);
	}
	if (c) {
		digitalWrite(pinC, HIGH);
	}
	else {
		digitalWrite(pinC, LOW);
	}
	if (d) {
		digitalWrite(pinD, HIGH);
	}
	else {
		digitalWrite(pinD, LOW);
	}
}
