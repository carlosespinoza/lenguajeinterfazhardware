﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WFPDemoG1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();//No se debe de borrar
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            btnClick.Content = "Ya me presionaste";
            MessageBox.Show($"Hola {txbNombre.Text}", "Demo WPF", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btnResultado_Click(object sender, RoutedEventArgs e)
        {
            float n1=float.Parse(txbNumero1.Text);
            float n2=float.Parse(txbNumero2.Text);
            float r;
            switch (((ComboBoxItem) cmbOperador.SelectedItem).Content.ToString())
            {
                case "+":
                    r = n1 + n2;
                    break;
                case "-":
                    r = n1- n2;
                    break;
                case "*":
                    r = n1 * n2;
                    break;
                case "/":
                    r = n1 / n2;
                    break;
                default:
                    r = 0;
                    break;
            }
            lblResultado.Content = $"Resultado: {r}";

        }
    }
}
