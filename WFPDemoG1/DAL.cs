﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFPDemoG1.Modelos;

namespace WFPDemoG1
{
    public class DAL
    {
        //Create Read Update Delete
        List<Contacto> Contactos;
        public DAL()
        {
            Contactos = new List<Contacto>();
            Contacto c = new Contacto();
            c.Apellidos = "Trejo Chavez";
            c.Email = "apaola@mail.com";
            c.FechaNacimiento = new DateTime(2002, 04, 01);
            c.Id = Guid.NewGuid().ToString();
            c.Telefono = "1234567890";
            c.Nombre = "Ana Paola";
            c.Password = "qwertyuiop";
            Contactos.Add(c);
            Contactos.Add(new Contacto()
            {
                Apellidos = "Olvera Leon",
                Nombre = "Emmanuel Alejandro",
                Email = "eolvera@mail.com",
                FechaNacimiento = new DateTime(2000, 10, 24),
                Id = Guid.NewGuid().ToString(),
                Telefono = "654321",
                Password = "poiuytrewq"
            });

        }

        public bool Create(Contacto item)
        {
            try
            {
                item.Id = Guid.NewGuid().ToString();
                Contactos.Add(item);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Contacto> Read
        {
            get
            {
                return Contactos;
            }
        }

        public bool Update(Contacto item)
        {
            try
            {
                Contacto c = Contactos.Find(e => e.Id == item.Id);
                if (c != null)
                {
                    c.Apellidos = item.Apellidos;
                    c.Email = item.Email;
                    c.FechaNacimiento = c.FechaNacimiento;
                    c.Nombre = item.Nombre;
                    c.Telefono = item.Telefono;
                    c.Password = item.Password;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete (Contacto item)
        {
            try
            {
                Contactos.Remove(item);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
