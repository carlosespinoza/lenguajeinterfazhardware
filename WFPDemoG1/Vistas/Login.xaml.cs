﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WFPDemoG1.Modelos;

namespace WFPDemoG1.Vistas
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        DAL dal;
        public Login()
        {
            InitializeComponent();
            dal = new DAL();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txbCorreo.Text))
            {
                if (!string.IsNullOrEmpty(pswPasword.Password))
                {
                    //select * from contactos where correo="" and password=""
                    Contacto r=dal.Read.SingleOrDefault(c => c.Email == txbCorreo.Text && c.Password == pswPasword.Password);
                    if (r != null)
                    {
                        MessageBox.Show($"Bienvenido {r.Nombre} {r.Apellidos}", "Login", MessageBoxButton.OK, MessageBoxImage.Information);
                        ListaDeContactosView v = new ListaDeContactosView(r);
                        v.Show();
                        Close();
                    }
                    else
                    {
                        MessageBox.Show($"Credenciales invalidas", "Login", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
    }
}
