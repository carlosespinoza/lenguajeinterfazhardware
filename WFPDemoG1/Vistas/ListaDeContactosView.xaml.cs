﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WFPDemoG1.Modelos;

namespace WFPDemoG1.Vistas
{
    /// <summary>
    /// Lógica de interacción para ListaDeContactosView.xaml
    /// </summary>
    public partial class ListaDeContactosView : Window
    {
        DAL dal;
        bool esNuevo;
        public ListaDeContactosView(Contacto contacto)
        {
            InitializeComponent();
            lblUsuario.Content = $"Bienvenid@: {contacto.Nombre} {contacto.Apellidos}";
            dal = new DAL();
            dtgDatos.ItemsSource = dal.Read;
            wrpDatos.DataContext = new Contacto();
            esNuevo = false;

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            wrpDatos.DataContext = new Contacto();
            esNuevo = true;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Contacto c = dtgDatos.SelectedItem as Contacto;
            if (c != null)
            {
                wrpDatos.DataContext = c;
                esNuevo = false;
            }
            else
            {
                MessageBox.Show("Primero selecciona un contacto");
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            Contacto c = wrpDatos.DataContext as Contacto;
            if (esNuevo)
            {
                if (dal.Create(c))
                {
                    MessageBox.Show("Elemento Creado");
                    ActualizarTabla();
                }
                else
                {
                    MessageBox.Show("Elemento NO Creado");
                }
            }
            else
            {
                if (dal.Update(c))
                {
                    MessageBox.Show("Elemento Modificado");
                    ActualizarTabla();
                }
                else
                {
                    MessageBox.Show("Elemento NO Modificado");
                }
            }
        }

        private void ActualizarTabla()
        {
            wrpDatos.DataContext = null;
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = dal.Read;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Contacto c = dtgDatos.SelectedItem as Contacto;
            if (c != null)
            {
                if (dal.Delete(c))
                {
                    MessageBox.Show("Elemento Eliminado");
                    ActualizarTabla();
                }
                else
                {
                    MessageBox.Show("Elemento NO Eliminado");
                }
            }
            else
            {
                MessageBox.Show("Primero selecciona un contacto");
            }
        }
    }
}
