﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DemoUIESP32
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SerialPort port;
        string contenido = "";
        DispatcherTimer timer;
        public MainWindow()
        {
            InitializeComponent();
            cmbPuertos.ItemsSource = SerialPort.GetPortNames();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 1);
            timer.Tick += Timer_Tick;
            
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (contenido != "")
            {
                EscribirEnLog(contenido);
                contenido = "";
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                port = new SerialPort(cmbPuertos.Text);
                port.BaudRate = 9600;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.DataBits = 8;
                port.DataReceived += Port_DataReceived;
                port.Open();
                timer.Start();
                EscribirEnLog("Puerto Abierto");
            }
            catch (Exception ex)
            {
                EscribirEnLog("Error: " + ex.Message);
            }
            
        }

        private void EscribirEnLog(string v)
        {
            lstLog.Items.Add(v);
        }

        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            contenido=port.ReadExisting();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                if(port!=null && port.IsOpen)
                {
                    port.Close();
                    timer.Stop();
                    EscribirEnLog("Puerto cerrado");
                }
                else
                {
                    EscribirEnLog("No se puede cerrar el puerto");
                }
                
            }
            catch (Exception ex)
            {
                EscribirEnLog("Error: " + ex.Message);
            }
        }

        private void btnEncender_Click(object sender, RoutedEventArgs e)
        {
            if (port.IsOpen)
            {
                port.WriteLine("L1");
            }
        }

        private void btnApagar_Click(object sender, RoutedEventArgs e)
        {
            if (port.IsOpen)
            {
                port.WriteLine("L0");
            }
        }

        private void btnGirarIzq_Click(object sender, RoutedEventArgs e)
        {
            if (port.IsOpen)
            {
                port.WriteLine("MI");
            }
        }

        private void btnDetener_Click(object sender, RoutedEventArgs e)
        {
            if (port.IsOpen)
            {
                port.WriteLine("MS");
            }
        }

        private void btnGirarDer_Click(object sender, RoutedEventArgs e)
        {
            if (port.IsOpen)
            {
                port.WriteLine("MD");
            }
        }
    }
}
